/**
 * http接口API集中管理
 */
const install = (Vue, vm) => {
	// 小程序通用的接口
	let wxLogin = (code) => vm.$u.get('/wxLogin/' + code);
	let wxGetUserInfo = (params = {}) => vm.$u.post('/wxGetUserInfo', params);
	let wxGetPhoneNumber = (params = {}) => vm.$u.post('/wxGetPhoneNumber', params);

	vm.$u.api = {
		wxLogin,
		wxGetUserInfo,
		wxGetPhoneNumber,
	}
}

export default {
	install
}
